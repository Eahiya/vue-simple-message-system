import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false


import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'


import router from '@/router/router'

import moment from 'moment';
Vue.prototype.moment = moment

import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
