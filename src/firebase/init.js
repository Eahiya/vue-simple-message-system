
import firebase from 'firebase'


const firebaseConfig = {
    apiKey: "AIzaSyAjvGe3uMU3WWK9fM_R7xhi_YJ5orUPb8I",
    authDomain: "chatapp-7c47c.firebaseapp.com",
    databaseURL: "https://chatapp-7c47c.firebaseio.com",
    projectId: "chatapp-7c47c",
    storageBucket: "chatapp-7c47c.appspot.com",
    messagingSenderId: "609663883444",
    appId: "1:609663883444:web:200945cf0fb09b8204dfe1",
    measurementId: "G-3RG1QS793Y"
  };


  const firebaseApp = firebase.initializeApp(firebaseConfig);

  export default firebaseApp.firestore()