import Vue from 'vue'

import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Home from '../pages/Home'
import Chat from '../pages/Chat'
const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/chat',
        component: Chat,
        name:'Chat',
        props: true,
        beforeEnter: (to, from, next) => {
            if(to.params.name){
                next()
            }else{
                // next('/')
                next({name: 'home'})
            }
            
        }

    }
]

const router = new VueRouter({
    routes,
    mode: 'history'
});

export default router;



